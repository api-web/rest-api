# Project Title

MS3 REST API Integrated with Angular


## Getting Started

MS3 project is implemented using java(spring-boot) for back-end and
and angular for front-end. The project is integrated with angular and can be run in local. 

### Details

```
-> Database Schema :

  Identification 
	   id  - int, primary key , auto increment 
	   dob - date,
	   fname - varchar,
	   gender - varchar,
	   lastname varchar,
	   title varchar,
	   
   Address 
	  id - int, primary key ,auto increment 
	  city - varchar
	  contact_id- int, foreign key
	  number - varchar
	  state - varchar
	  street - varchar
	  type - varchar
	  unit - varchar
	  zipcode - varchar
	  
	Communication
	  id - int, primary key ,auto increment 
	  contact_id - int, foreign key
	  preferred - boolean
	  type - varchar
	  value - varchar
	  
	  
	  
-> valid JSON :
	{
	   "Identification":{
		  "gender":"Male",
		  "dateOfBirth":"2020-10-17",
		  "firstName":"john",
		  "lastName":"smith",
		  "title":"manager"
	   },
	   "Address":[
		  {
			 "type":"home",
			 "number":23451,
			 "street":"Gomez Street",
			 "unit":"bldg 101",
			 "city":"cebu",
			 "state":"US",
			 "zipCode":"4566"
		  }
	   ],
	   "Communication":[
		  {
			 "preferred":true,
			 "type":"email",
			 "val":"john.smith@gmail.com"
		  }
	   ]
	}
  
  Note : To save Contact data :  put an id value in identification  . Check the sample json below :
  
 "Identification":{
		  "id" : 1,
		  "gender":"Male",
		  "dateOfBirth":"2020-10-17",
		  "firstName":"john",
		  "lastName":"smith",
		  "title":"manager"
	   }
	   
	   
	   
 -> Data in the form should be complete. Atleast 1 information for address and communication to save the data.
 
```
### Installing

```
database - mysql :
-> in rest-api\src\main\resources\application.properties - Change the connection String based on your environment.
   in my case I used xampp to access the mysql database and run it.
   
   example :
   spring.datasource.url=jdbc:mysql://localhost:3306/ms3
   spring.datasource.username=root
   spring.datasource.password=
   
   
   
-> Install java :
   jdk version : jdk-15
   
   make sure you have JAVA_HOME in environment variables -> user variables
   
   sample :
	"user variable name"  set  JAVA_HOME
	"user variable name" set to C:\Program Files\Java\jdk

-> Install maven :
	maven version : apache-maven-3.6.3
	
Note : it is not neccessary to run mvn clean install on the project.
       it will cause the static files from angular will be deleted.
```


## MYSQL

```

CREATE DATABASE IF NOT EXISTS ms3;
USE ms3;
-- --------------------------------------------------------

--
-- Table structure for table `contact_identification`
--

CREATE TABLE IF NOT EXISTS `contact_identification` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `dob` date DEFAULT NULL,
  `fname` varchar(150) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `lastname` varchar(150) DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
      PRIMARY KEY (id)
) ;


--
-- Table structure for table `contact_address`
--



CREATE TABLE IF NOT EXISTS `contact_address` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `city` varchar(255) DEFAULT NULL,
  `contact_id` int(11) NOT NULL,
  `number` varchar(20) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `unit` varchar(100) DEFAULT NULL,
  `zipcode` varchar(50) DEFAULT NULL,
    PRIMARY KEY (id),
   FOREIGN KEY (contact_id) 
   REFERENCES contact_identification(id) on DELETE CASCADE
)  ENGINE=InnoDB;
  
-- --------------------------------------------------------

--
-- Table structure for table `contact_communication`
--

CREATE TABLE IF NOT EXISTS `contact_communication` (
`id` int(11) NOT NULL AUTO_INCREMENT,
  `contact_id` int(11) NOT NULL,
  `preferred` bit(1) DEFAULT NULL,
  `type` varchar(100) DEFAULT NULL,
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (id),
      FOREIGN KEY (contact_id)
   REFERENCES contact_identification(id) on DELETE CASCADE
) ENGINE=InnoDB ;



-- --------------------------------------------------------


```

### API

```
    GET : /api/contacts -> get all list of contacts (identification)
	GET : /api/contact/{id} -> get contact details by id
	POST: /api/contact -> save contact details
	PUT : /api/contact -> update contact 
	DELETE : /api/contact/{id} -> delete contact details
```

### Run the application

```
 1. Copy and paste the script of MYSQL then execute. It will create the ms3 database.
 2. Check the connection String on the "application.properties"
 3. Check the database connection is running.
 4. Open a command line
 5. Go to the project with pom.xml (cd project)
 6. run this command :
    mvn spring-boot:run
	This command will  run the application.
	
 7. Access :
 http://localhost:8080/#/home - client 
 http://localhost:8080/api/contacts - API
 
```

### How to use the client website

```
 1. open http://localhost:8080/#/home
	
	 To create :
	 
	  - click "Add"
	  - Input data on the fields of identification
	  - To add new address, input the fields of address then click the "Add new address" button
	  - To add new communication, input the fields of communication then click the "Add new communication" button
	  - To save the Contact , click sumbmit button, atleast 1 address and communication should be provided
	
	To View :
	
	  - click "List of contacts"
	  - list of identification will display with on the screen
	  - click the "view" label button to view the details of the specific person
	 
	To Edit :
	
	  - click "list of contacts"
	  - select the specific person to edit the details of contact
	  - click the "edit" label button to edit the details
	  - It will redirect to edit page and automatically fill up the fields. You can remove the address details by
		clicking the "close" icon.
	  - After done edting click the "update" button to update the contact details.
	  
	To Delete :
	  
	  - click "list of contacts"
	  - select the specific person you want to remove.
	  - click the "delete" label button.
 
```
