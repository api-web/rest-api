package com.api.ms3.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.ms3.DTO.ContactDTO;
import com.api.ms3.DTO.IdentificationDTO;
import com.api.ms3.manager.IContactService;

@RestController
@RequestMapping("/api")
public class ContactController {
	


	@Autowired
	IContactService contactService;
	
	String success = "Success";
	
	// get the list of contact
	@RequestMapping(value = "/contacts", method = RequestMethod.GET)
	public ResponseEntity<List<IdentificationDTO>> getContacts(){      
		 return ResponseEntity.ok().body(contactService.getIdentificationList());
    }

	// get the list of contact
	@RequestMapping(value = "/contact/{id}", method = RequestMethod.GET)
	public ResponseEntity<ContactDTO> getContactById(@PathVariable(value = "id") Integer id){      
		  return ResponseEntity.ok().body(contactService.getContactById(id));
    }

	
	// save contact
    @PostMapping("/contact")
	public void addContact (@Valid @RequestBody ContactDTO contactDTO) {
    	contactService.save(contactDTO);
    			
	}
    
    // update contact
    @PutMapping("/contact")
  	public void updateContact (@Valid @RequestBody ContactDTO contactDTO) {
      	contactService.save(contactDTO);
  	}

    // delete Contact record
 	@RequestMapping(value = "/contact/{id}", method = RequestMethod.DELETE)
	public void deleteContact(@PathVariable(value = "id") int id){
 		 contactService.delete(id);

	}

}
