package com.api.ms3.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CommunicationDTO {
	
	// json  format
    @JsonProperty(value ="id")
    private int id;
    
    @JsonProperty(value ="type")
    private String type;
    
    @JsonProperty(value ="val")
    private String value;

    @JsonProperty(value ="preferred")
    private boolean preferred;
    
    // getters and setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isPreferred() {
		return preferred;
	}

	public void setPreferred(boolean preferred) {
		this.preferred = preferred;
	}

	
}
