package com.api.ms3.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IdentificationDTO {

	// json  format
    @JsonProperty(value ="id")
    private int id;
    
    @JsonProperty(value ="firstName")
    private String firstname;
    
    @JsonProperty(value ="lastName")
    private String lastname;
    
    @JsonProperty(value ="dateOfBirth")
    private String dob;
    
    @JsonProperty(value ="gender")
    private String gender;
    
    @JsonProperty(value ="title")
    private String title;

    // getters and setters
    
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
    
}
