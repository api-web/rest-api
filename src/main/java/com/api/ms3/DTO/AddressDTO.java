package com.api.ms3.DTO;

import com.fasterxml.jackson.annotation.JsonProperty;

public class AddressDTO {
	
	// json  format
    @JsonProperty(value ="id")
    private int id;
    
    @JsonProperty(value ="contact_id")
    private int contact_id;

    @JsonProperty(value ="type")
    private String type;
    
    @JsonProperty(value ="number")
    private String number;
    
    @JsonProperty(value ="street")
    private String street;
    
    @JsonProperty(value ="unit")
    private String unit;
    
    @JsonProperty(value ="city")
    private String city;
    
    @JsonProperty(value ="state")
    private String state;
     
	@JsonProperty(value ="zipCode")
    private String zipcode;

    
    // getters  and  setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	 public int getContact_id() {
			return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}


    
}
