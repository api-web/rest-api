package com.api.ms3.DTO;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ContactDTO {
	
	// json  format
    @JsonProperty(value ="Identification")
    private IdentificationDTO identification;

    @JsonProperty(value ="Address")
    private List<AddressDTO> address;
    
    @JsonProperty(value ="Communication")
    private List<CommunicationDTO> communication;
    
    // getters and setters
	public IdentificationDTO getIdentification() {
		return identification;
	}

	public void setIdentification(IdentificationDTO identification) {
		this.identification = identification;
	}

	public List<AddressDTO> getAddress() {
		return address;
	}

	public void setAddress(List<AddressDTO> address) {
		this.address = address;
	}

	public List<CommunicationDTO> getCommunication() {
		return communication;
	}

	public void setCommunicaton(List<CommunicationDTO> communication) {
		this.communication = communication;
	}

 
    
}
