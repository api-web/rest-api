package com.api.ms3.DAO;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "contact_identification")
@DynamicUpdate
public class Identification implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* id */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "fname")
	private String firstname;

	@Column(name = "lastname")
	private String lastname;

	@Column(name = "dob")
	private Date dob;

	@Column(name = "gender")
	private String gender;

	@Column(name = "title")
	private String title;

	// constructors
	public Identification() {
	}


	// getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getfirstname() {
		return firstname;
	}

	public void setfirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	// auto generated to String
	@Override
	public String toString() {
		return "Identification [id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", dob=" + dob + ", gender="
				+ gender + ", title=" + title + "]";
	}
	
	

}
