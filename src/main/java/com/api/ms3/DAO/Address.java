package com.api.ms3.DAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "contact_address")
@DynamicUpdate
public class Address implements Serializable  {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* id */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name = "contact_id")
	private int contact_id;

	@Column(name = "type")
	private String type;

	@Column(name = "number")
	private String number;

	@Column(name = "street")
	private String street;

	@Column(name = "unit")
	private String unit;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;

	@Column(name = "zipcode")
	private String zipcode;

	// constructors
	public Address() {
	}

	public Address(int id, int contact_id, String type, String number, String street, String unit, String city,
			String state, String zipcode) {
		
		this.id = id;
		this.contact_id = contact_id;
		this.type = type;
		this.number = number;
		this.street = street;
		this.unit = unit;
		this.city = city;
		this.state = state;
		this.zipcode = zipcode;
	}

	// getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContact_id() {
		return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	// auto generated to String
	@Override
	public String toString() {
		return "Address [id=" + id + ", contact_id=" + contact_id + ", type=" + type + ", number=" + number
				+ ", street=" + street + ", unit=" + unit + ", city=" + city + ", state=" + state + ", zipcode="
				+ zipcode + "]";
	}

	
}
