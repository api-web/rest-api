package com.api.ms3.DAO;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

@Entity
@Table(name = "contact_communication")
@DynamicUpdate
public class Communication implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/* id */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;

	@Column(name = "contact_id")
	private int contact_id;

	@Column(name = "type")
	private String type;

	@Column(name = "value")
	private String value;

	@Column(name = "preferred")
	private boolean preferred;

	// constructors
	public Communication() {
	}

	public Communication(int id, int contact_id, String type, String value, boolean preferred) {
		this.id = id;
		this.contact_id = contact_id;
		this.type = type;
		this.value = value;
		this.preferred = preferred;
	}

	// getters and setters
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getContact_id() {
		return contact_id;
	}

	public void setContact_id(int contact_id) {
		this.contact_id = contact_id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean getPreferred() {
		return preferred;
	}

	public void setPreferred(boolean preferred) {
		this.preferred = preferred;
	}

	// auto generated to String
	@Override
	public String toString() {
		return "Communication [id=" + id + ", contact_id=" + contact_id + ", type=" + type + ", value=" + value
				+ ", preferred=" + preferred + "]";
	}
	
	
	
	
	
	

}
