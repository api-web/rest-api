package com.api.ms3.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import com.api.ms3.DAO.Address;
import com.api.ms3.DAO.Communication;
import com.api.ms3.DAO.Identification;
import com.api.ms3.DTO.AddressDTO;
import com.api.ms3.DTO.CommunicationDTO;
import com.api.ms3.DTO.ContactDTO;
import com.api.ms3.DTO.IdentificationDTO;
import com.api.ms3.manager.IContactService;
import com.api.ms3.repository.IAddress;
import com.api.ms3.repository.ICommunication;
import com.api.ms3.repository.IIdentification;

@Service
public class ContactServices implements IContactService {

	@Autowired
	IIdentification identificationRepository;
	
	@Autowired
	IAddress addressRepository;
	
	@Autowired
	ICommunication communicationRepository;
	
	ModelMapper mapper;
	List<Address> addresses = new ArrayList<>();
	List<Communication> communications  = new ArrayList<>();
	
	
	// get Identification List
	@Override
	public List<IdentificationDTO> getIdentificationList(){
		// TODO Auto-generated method stub
		
		mapper = new ModelMapper();
		List<IdentificationDTO> identinDTO = new ArrayList<>();
		List<Identification> identificationList  = identificationRepository.findAll();


		if(!identificationList.isEmpty()) {
			identinDTO = Arrays.asList(mapper.map(identificationList, IdentificationDTO[].class));

		}
	
	
		return identinDTO;
	}

	// save Contact
	@Transactional
	@Override
	public void save(ContactDTO contactDTO) {
		// TODO Auto-generated method stub

		int id = saveIdentification(contactDTO);
		
		saveAddressAndCom(contactDTO, id);
	}
	

	// save / update identification
	private int saveIdentification(ContactDTO contactDTO) {
		mapper = new ModelMapper();
		Identification iden = new Identification();
		// map the DTO to objects
		mapper.map(contactDTO.getIdentification(), iden);
		
		identificationRepository.save(iden);
		
	    return iden.getId();
	}
	
	
	// save address and communication
	private void saveAddressAndCom(ContactDTO contactDTO, int id) {

		List<Address> mapAddress = new ArrayList<>();
		List<Communication> mapCommunication = new ArrayList<>();
		mapper = new ModelMapper();
		
		// Address DTO format to object
		mapAddress = mapper.map(contactDTO.getAddress(), new TypeToken<List<Address>>() {}.getType());


		List<Address> dbAddress = addressRepository.findAllAddress(id);

		// loop from db address
		for(Address dbAddr : dbAddress){

			if (!mapAddress.stream().anyMatch(o -> o.getId() == dbAddr.getId() )) {
				// delete
				addressRepository.deleteById(dbAddr.getId());
			}
				
		
		}
		
		// loop DTO address
		for(Address add : mapAddress) {
			if(add.getId() == 0) {
				add.setContact_id(id);
				// save
				addressRepository.save(add);

			}
		}
		
			
		
		// Communication DTO format to object
		mapCommunication = mapper.map(contactDTO.getCommunication(), new TypeToken<List<Communication>>() {}.getType());
		
		List<Communication> dbComm = communicationRepository.findAllCommunication(id);

		// loop from db address
		for(Communication dbCom : dbComm){

			if (!mapCommunication.stream().anyMatch(o -> o.getId() == dbCom.getId() )) {
				// delete
				communicationRepository.deleteById(dbCom.getId());
			}
				
		
		}
		
		// loop DTO address
		for(Communication com : mapCommunication) {
			if(com.getId() == 0) {

				com.setContact_id(id);
				communicationRepository.save(com);

			}
		}
	

	}
	
	// get Contact Details
	@Override
	public ContactDTO getContactById(int id) {
		// TODO Auto-generated method stub
		mapper = new ModelMapper();
		ContactDTO contactDTO = new ContactDTO();
		IdentificationDTO idenDTO = new IdentificationDTO();
		List<AddressDTO> addressDTO = new ArrayList<>();
		List<CommunicationDTO> communicationDTO = new ArrayList<>();
		
		// get identification based on id
		Identification iden = identificationRepository.findByIdentifictionId(id);
		
		if(!ObjectUtils.isEmpty(iden)){
			// map the data to Identification DTO
			idenDTO = mapper.map(iden, IdentificationDTO.class);	
			// set
			contactDTO.setIdentification(idenDTO);
		}

		// get all address
		addresses = addressRepository.findAllAddress(id);
		if(!addresses.isEmpty()) {
			addressDTO = Arrays.asList(mapper.map(addresses, AddressDTO[].class));
			contactDTO.setAddress(addressDTO);
		}
		
	
		// get all address
		communications = communicationRepository.findAllCommunication(id);
		if(!communications.isEmpty()) {
			communicationDTO = Arrays.asList(mapper.map(communications, CommunicationDTO[].class));
			contactDTO.setCommunicaton(communicationDTO);
		}


	
		return contactDTO;
	}

	// delete Contact
	@Transactional
	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		Optional<Identification> iden  = identificationRepository.findById(id);
		
		if(!ObjectUtils.isEmpty(iden)) {
			// delete
			identificationRepository.deleteById(id);
			addressRepository.deleteAddresses(id);
			communicationRepository.deleteComms(id);
		}
	}
	
	
	
		
}
