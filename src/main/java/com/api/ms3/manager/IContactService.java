package com.api.ms3.manager;

import java.util.List;

import com.api.ms3.DTO.ContactDTO;
import com.api.ms3.DTO.IdentificationDTO;

public interface IContactService {

   List<IdentificationDTO> getIdentificationList();
   void save(ContactDTO contactDTO); // add and update Contact
   ContactDTO getContactById(int id);
   void delete(int id); // delete Contact
}
