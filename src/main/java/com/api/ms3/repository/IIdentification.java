package com.api.ms3.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.api.ms3.DAO.Identification;

@Repository
public interface IIdentification extends JpaRepository <Identification, Integer> {
	
	// queries here
	@Query(
			  value = "SELECT * FROM contact_identification WHERE id = ?", 
			  nativeQuery = true)
			Identification findByIdentifictionId(Integer contact_id);
}
