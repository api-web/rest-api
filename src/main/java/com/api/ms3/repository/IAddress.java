package com.api.ms3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.api.ms3.DAO.Address;
import com.api.ms3.DAO.Communication;

@Repository
public interface IAddress  extends JpaRepository<Address, Integer> {

	// get all address based on contact id 
	@Query(
			  value = "SELECT * FROM contact_address WHERE contact_id = ?", 
			  nativeQuery = true)
			List<Address> findAllAddress(Integer contact_id);
	
//	// update data
//	@Modifying(clearAutomatically = true)
//	@Query(
//			  value = "UPDATE contact_address set  type = ?, number = ?, street = ?, " +
//					  "unit = ? , city = ? , state = ? , zipcode = ? " + 
//					  "WHERE contact_id = ? and id = ?", 
//			  nativeQuery = true)
//		 void updateAddress(String type, int number, String street, String unit, String city, String state, String zipcode,int contact_id, int id);

	// delete data
	@Modifying(clearAutomatically = true)
	@Query(
			  value = "DELETE from contact_address WHERE contact_id = ? ",
			  nativeQuery = true)
		 void deleteAddresses(int contact_id);

			
}
