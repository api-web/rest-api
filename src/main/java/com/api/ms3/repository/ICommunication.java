package com.api.ms3.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.api.ms3.DAO.Communication;

@Repository
public interface ICommunication extends JpaRepository<Communication, Integer> {

	// get all communications based on contact id 
	@Query(
			  value = "SELECT * FROM contact_communication WHERE contact_id = ?", 
			  nativeQuery = true)
			List<Communication> findAllCommunication(Integer contact_id);
	
	// delete data
	@Modifying(clearAutomatically = true)
	@Query(
			  value = "DELETE from contact_communication WHERE contact_id = ? ",
			  nativeQuery = true)
		 void deleteComms(int contact_id);


}
